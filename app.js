const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')

const routes = require('./routes/routes')

const app = express();

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}))
app.use(bodyParser.json())
app.use(cors());

app.use('/', routes)

//Open connection
app.listen(process.env.PORT, () => {
    console.log(`LISTEN ON PORT : ${process.env.PORT}`)
});
