
exports.sayhello = (req, res, next) => {
    return res.status(200).json({
        msg: 'Hello Word!'
    })
}

exports.helloname = (req, res, next) => {
    let name = req.params.name
    return res.status(200).json({
        msg: `Hello ${name}, wellcome!`
    })
}