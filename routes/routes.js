const express = require('express')
const router = express.Router()

const controller = require('../controller/controller')

router.get('/', controller.sayhello)
router.get('/:name', controller.helloname)

module.exports = router;